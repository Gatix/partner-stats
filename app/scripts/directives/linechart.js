'use strict';

angular.module('stats2App')
  .directive('lineChart', function () {
    return {
      restrict: 'E',
      scope: {
        data: '='
      },
      template: '<div class="chart"><svg></svg></div>',
      link: function postLink(scope, element, attrs) {
        scope.$watch('data', function(data){
         nv.addGraph(function() {
             var chart = nv.models.lineChart();

             chart.xAxis
                 .axisLabel('Time (ms)')
                 .tickFormat(d3.time.format("%Y-%m-%d"));

             chart.yAxis
                 .axisLabel('Voltage (v)')
                 .tickFormat(d3.format('.0%'));

             d3.select('.chart svg')
                 .datum(data)
               .transition().duration(500)
                 .call(chart);

             nv.utils.windowResize(function() { d3.select('#chart svg').call(chart) });

             return chart;
           });
        });
      }
    }
  });
