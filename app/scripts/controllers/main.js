'use strict';

angular.module('stats2App')
  .controller('MainCtrl', function ($scope, $http, $filter) {

    $scope.getNumber = function(num) {
      return new Array(num);
    }

    $scope.triggerResize = function () {
      jQuery(window).trigger('resize');
    }

    $scope.statsData = [] ;

    $scope.filteredData = [];

    // Go through each site
    function filterStatsData() {

    // Reverse order of sites
    $scope.statsData.reverse();

    /* Site loop */
    for (var i=0; i < $scope.statsData.length; i++){

      // var name = site.name;
      var site = $scope.statsData[i];
          site.chartData = {};

      site.chartData.conversionRate = [];
      site.chartData.sales = [];
      site.chartData.leads = [];

      // Formats
      var dateFormat = d3.time.format("%Y-%m-%d");
      var percentage = d3.format(".0%");

      // Reverse order of campaigns
      site.campaigns.reverse();

      /* Campaigns loop */
      for (var j=0; j < site.campaigns.length; j++){
        var campaigns = site.campaigns[j];

        // Skip if empty
        if (campaigns.data == undefined)
          continue;

        var campaignName = campaigns.type.charAt(0).toUpperCase() + campaigns.type.slice(1);

        var salesCRData = {};
        salesCRData.key = campaignName + ' Sales CR';
        salesCRData.values = [];

        var leadsCRData = {};
        leadsCRData.key = campaignName + ' Leads CR';
        leadsCRData.values = [];

        var salesData = {};
        salesData.key = campaignName + ' Sales';
        salesData.values = [];

        var leadsData = {};
        leadsData.key = campaignName + ' Leads';
        leadsData.values = [];


        var campaignTotals = { leads: 0, sales: 0, clicks: 0 };

        /* Data loop */
        for (var k=0; k < campaigns.data.length; k++){
          var cData = campaigns.data[k];
          var salesCRValues = [],
              leadsCRValues = [],
              salesValues = [],
              leadsValues = [],
              date = dateFormat.parse(cData.date); // returns a Date

          var salesCR = 0.000, leadsCR = 0.000;

          if (cData.sales != 0 && cData.leads != 0 && cData.clicks != 0) {
            /* Sales CR */
            salesCR = cData.sales / cData.leads;
            /* Leads CR */
            leadsCR = cData.leads / cData.clicks;
          } else { console.log('NOT'); }

          salesCRValues = [ date, salesCR ];
          leadsCRValues = [ date, leadsCR ];
          salesValues = [ date, cData.sales ];
          leadsValues = [ date, cData.leads ];

          /* Totals */
          campaignTotals.leads += parseInt(cData.leads);
          campaignTotals.sales += parseInt(cData.sales);
          campaignTotals.clicks += parseInt(cData.clicks);

          // Push to data arrays
          salesCRData.values.push(salesCRValues);
          leadsCRData.values.push(leadsCRValues);
          salesData.values.push(salesValues);
          leadsData.values.push(leadsValues);

        }

        campaigns.totalData = campaignTotals;

        // Add campaigns
        site.chartData.conversionRate.push(salesCRData);
        site.chartData.conversionRate.push(leadsCRData);
        site.chartData.sales.push(salesData);
        site.chartData.leads.push(leadsData);

      };

    };

    } // end func

      $scope.xAxisTickFormat = function(){
        return function(d){
          return d3.time.format("%Y-%m-%d")(new Date(d));  //uncomment for date format
        }
      }
      $scope.xAxisTickValues = function(){
        return d3.time.day.range(
                            $scope.fromDate,
                            $scope.toDate);
      }
      $scope.xAxisScale = function() {
          return d3.time.scale();
      };
      $scope.yAxisTickFormat = function(){
        return function(d){
          var percentage = d3.format(".2%");
          return percentage(d);  //uncomment for date format
        }
      }
      $scope.lineColors = function() {
        return function(d,i){
          var color = d3.scale.category10();
          return color(i);
        }
      };

      function formatDate (date) {
        return $filter('date')(date, 'yyyy-MM-dd');
      }
      $scope.setDate = {
        yesterday : function() {
          var yesterdayDate = moment().subtract('days', 1);
          $scope.fromDate = yesterdayDate.format('YYYY-MM-DD');
          $scope.toDate = yesterdayDate.format('YYYY-MM-DD');
        },
        lastWeek : function() {
          $scope.fromDate = moment().subtract('weeks', 1).startOf('week').format('YYYY-MM-DD');
          $scope.toDate = moment().subtract('weeks', 1).endOf('week').format('YYYY-MM-DD');
        },
        last7Days : function() {
          this.yesterday();
          $scope.fromDate = moment().subtract('days', 1).subtract('days', 7).format('YYYY-MM-DD');
        },
        lastMonth : function() {
          $scope.fromDate = moment().subtract('months', 1).startOf('month').format('YYYY-MM-DD');
          $scope.toDate = moment().subtract('months', 1).endOf('month').format('YYYY-MM-DD');
        }
      };

      $scope.maxDate = moment().subtract('days', 1);
      $scope.setDate.last7Days();

      $scope.updateData = function() {

        var dateDiff = (new Date($scope.toDate) - new Date($scope.fromDate)) / (1000*60*60*24);
        if (dateDiff > 31) {
          $('.datepickers').addClass('has-error');
          alert("Maximum of 31 days only")
          return;
        } else {
          $('.datepickers').removeClass('has-error');
        }

        $http({method: 'GET', url: 'http://test.gatix.dev/partner-stats.php?fromDate='+formatDate($scope.fromDate)+'&toDate='+formatDate($scope.toDate)}).
        // $http({method: 'GET', url: '/reports/dailyStatsSummaryJSON?start='+formatDate($scope.fromDate)+'&end='+formatDate($scope.toDate)}).
          success(function(data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
            console.log(data);
            $scope.statsData = data;
            filterStatsData();
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log(data);
          });

      }

    $scope.updateData();

});
