'use strict';

angular.module('stats2App', [
  // 'ngCookies',
  'ngResource',
  // 'ngSanitize',
  'ngRoute',
  'nvd3ChartDirectives',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.utils',
  'chieffancypants.loadingBar'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($location, $rootScope) {
  $rootScope.$on('$viewContentLoaded', function() {
     $rootScope.$watch(function() { return $location.search() }, function(search) {
       var scrollPos = 0
       if (search.hasOwnProperty('scroll')) {
         var $target = $('#' + search.scroll);
         var scrollPos = $target.offset().top;
       }
       $("body,html").animate({scrollTop: scrollPos}, "slow");
     });
   });
 });
